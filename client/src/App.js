import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { Button } from '@mui/material';
import AppBarComp from './components/AppBarComp';
import FooterComp from './components/FooterComp';
import './App.css';
import { Link } from "react-router-dom";
import axios from 'axios';


function Home() {
  const [mensaje, setMensaje] = React.useState('');

  React.useEffect(() => {
    async function obtenerMensaje() {
      await axios.get('http://127.0.0.1:3001/msj')
        .then((res) => {
          if (res.statusText === 'OK') {
            setMensaje(res.data);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    obtenerMensaje()
  }, []);
  return (
    <>
      <AppBarComp />
      <Box sx={{ flexGrow: 1 }}>
        <Grid container direction="column"
          justifyContent="center"
          alignItems="center">
          <Grid item xs={9}>
            <img src="/images/mlo.jpeg" alt="" width={"400px"} />
          </Grid>
          <Grid item xs={9}>
            <h3>{mensaje}</h3>
          </Grid>
          <Grid item xs={9}>
            <Button variant="outlined"><Link to={`proveedores`}>Continuar</Link></Button>
          </Grid>
        </Grid>
      </Box>
      <br />
      <FooterComp />
    </>
  );
}

export default Home;
