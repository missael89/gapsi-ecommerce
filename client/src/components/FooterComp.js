import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import axios from 'axios';

function FooterComp() {
    const [version, setVersion] = React.useState('');

    React.useEffect(() => {

        async function obtenerVersion() {
            await axios.get('http://127.0.0.1:3001/version')
                .then((res) => {

                    if (res.statusText === 'OK') {
                        setVersion(res.data);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }

        obtenerVersion();

    }, []);

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container justifyContent={'center'} bgcolor="grey">
                <Grid item xs={9}>
                    <Typography
                        variant="subtitle1"
                        align="right"
                        color="text.secondary"
                        component="p"
                    >
                        versión {version}
                    </Typography>
                </Grid>
            </Grid>
        </Box>
    );
}

export default FooterComp;