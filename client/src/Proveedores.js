import * as React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
//import { Button } from '@mui/material';
import DataTable from 'react-data-table-component';
import AppBarComp from './components/AppBarComp';
import axios from 'axios';
import { Button, Input } from '@mui/material';

function Proveedores() {
    let [proveedores, setProveedores] = React.useState([]);
    let [reloadData, setReloadData] = React.useState(false);

    const columns = React.useMemo(() => [
        {
            cell: (row) => <Button variant="outlined" onClick={() => handleButtonDelete(row)} data-tag="allowRowEvents" id={row.id}>Eliminar</Button>,
            allowOverflow: true,
            button: true,
            width: "150px"
        },
        {
            name: 'Nombre',
            selector: (row) => row.nombre,
        },
        {
            name: 'Razón Social',
            selector: (row) => row.razonSocial,
        },
        {
            name: 'Dirección',
            selector: (row) => row.direccion,
        },
    ], []);

    const handleSumbitAdd = async (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const jsonData = {
            id: Math.floor(Math.random() * 1000), // Asignar un id aleotorio para agregar a objeto json de proveedores
            nombre: data.get('nombre'),
            razonSocial: data.get('razonSocial'),
            direccion: data.get('direccion')
        }

        await axios.put('http://127.0.0.1:3001/proveedores', jsonData)
            .then((res) => {
                if (res.statusText === 'OK') {
                    event.target.reset();
                    setReloadData(true);
                } else if (res.data === 'Ya existe el proveedor.') {
                    alert(res.data);
                } else {
                    console.log(res.data);
                } 
            }).catch((error) => {
                console.log(error)
            });
    }

    const handleButtonDelete = async (event) => {

        await axios.delete(`http://127.0.0.1:3001/proveedores/${event.id}`)
            .then((res) => {
                if (res.statusText === 'OK') {
                    setReloadData(true);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    React.useEffect(() => {
        async function getProveedores() {
            await axios.get('http://127.0.0.1:3001/proveedores')
                .then((res) => {
                    if (res.statusText === 'OK') {
                        let rows = res.data;

                        setProveedores(proveedores = rows);

                    } else {
                        console.log(res.data);
                    }
                }).catch((error) => {
                    console.log(error)
                });
        }

        getProveedores();

        setReloadData(false);

    }, [reloadData]);

    return (
        <>
            <AppBarComp />
            <Box sx={{ flexGrow: 1 }}>
                <Grid container justifyContent={'center'} direction="column" alignItems="center">
                    <Grid item xs={9}>
                        <form onSubmit={handleSumbitAdd}>

                            <h4>Agregar Proveedor</h4>
                            <Input type="text" placeholder="Nombre" name='nombre' id='nombre' required />
                            <br />
                            <Input type="text" placeholder="Razón Social" name='razonSocial' id='razonSocial' required />
                            <br />
                            <Input type="text" placeholder="Dirección" name='direccion' id='direccion' required />
                            <br />
                            <br />
                            <Button variant="outlined" type='submit'>Agregar</Button>
                        </form>
                    </Grid>
                </Grid>
                <Grid container justifyContent={'center'}>

                    <Grid item xs={9}>
                        <br />
                        <hr></hr>
                        <header>Lista de proveedores</header>
                        <hr></hr>
                        <br />
                        <DataTable
                            columns={columns}
                            data={proveedores}
                            striped
                            pagination
                            responsive
                            persistTableHead
                        />
                    </Grid>
                </Grid>
            </Box>
        </>
    );

}

export default Proveedores;